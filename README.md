# BucketUp

### About
---
**BucketUp** is a **easy** to use and _**fully featured** (work in progress)_ plugin for **Bukkit / CraftBukkit** based Minecraft servers (like [Spigot](https://www.spigotmc.org/))

This plugin implementation relies on [UpsetCore](https://www.github.com/upsettingboy/UpsetCore).

### Build
---
Building this project following this steps:
1. **Install Gradle** following the official guide: [Install Gradle](https://gradle.org/install/)
2. **Clone this repository** _(could be any branch)_
3. **Open a terminal** (Git Bash too) and _cd_ to the repo. Then type:
```groovy
gradle build
```
4. The build will be on **_build / libs_**
