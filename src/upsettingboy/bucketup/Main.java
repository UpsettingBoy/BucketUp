// Copyright (c) 2018 UpsettingBoy (Jerónimo Sánchez)
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package upsettingboy.bucketup;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import upsettingboy.bucketup.backup.*;
import upsettingboy.bucketup.commands.CMDForceBackup;
import upsettingboy.upsetcore.io.ConfigUtils;
import upsettingboy.upsetcore.io.FileUtils;

public class Main extends JavaPlugin 
{
	public static final String PLUGIN_FOLDER = FileUtils.getPluginsLocation() + File.separator + "BucketUp";
	
	public static final String CONFIG_FILE = PLUGIN_FOLDER + File.separator + "BucketUp.conf";
	
	public static final String PLUGIN_NAME = "BucketUp";
	
	private static Backup backup;
	
	@Override
	public void onEnable()
	{
		ConfigUtils.writeConfig(CONFIG_FILE, new BackupConfig.BackupConfigBuilder()
				.withLocation(FileUtils.getServerParentLocation() + File.separator + "BucketUp-Backups")
				.withWorldBackup(true).withWorldBackupInterval(600)
				.withCompleteBackup(false).withCompleteBackupInterval(1800)
				.build());
				
		backup = new Backup();
		backup.start();

		loadCommands();
	}
	
	@Override
	public void onDisable()
	{
		backup.forceBackup();
		backup.disableTimers();
	}

	private void loadCommands()
	{
		this.getCommand("fb").setExecutor(new CMDForceBackup());
	}

	public static Backup getBackupInstance()
	{
		return Main.backup;
	}
}