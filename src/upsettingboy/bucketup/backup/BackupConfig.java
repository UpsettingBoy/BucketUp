// Copyright (c) 2018 UpsettingBoy (Jerónimo Sánchez)
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package upsettingboy.bucketup.backup;

public class BackupConfig 
{
	private final String backupLocation;

	private final boolean worldBackup;
	
	private final long worlBackupInterval;
	
	private final boolean completeBackup;
	
	private final long completeBackupInterval;
	
	public BackupConfig(final String location, final boolean worldBackup, final long worldBackupInterval, final boolean completeBackup, final long completeBackupInterval)
	{
		this.backupLocation = location;
		this.worldBackup = worldBackup;
		this.worlBackupInterval = worldBackupInterval;
		this.completeBackup = completeBackup;
		this.completeBackupInterval = completeBackupInterval;
	}
		
	public String getBackupLocation() 
	{
		return backupLocation;
	}

	public boolean isWorldBackup() 
	{
		return worldBackup;
	}

	public long getWorlBackupInterval() 
	{
		return worlBackupInterval;
	}

	public boolean isCompleteBackup() 
	{
		return completeBackup;
	}

	public long getCompleteBackupInterval() 
	{
		return completeBackupInterval;
	}

	public static class BackupConfigBuilder
	{
		private String location;
		
		private boolean isWorldBackup;
		
		private long worldBackupInterval;
		
		private boolean isCompleteBackup;
		
		private long completeBackupInterval;
		
		public BackupConfigBuilder withLocation(String location)
		{
			this.location = location;
			return this;
		}
		
		public BackupConfigBuilder withWorldBackup(boolean isWorldBackup)
		{
			this.isWorldBackup = isWorldBackup;
			return this;
		}
		
		public BackupConfigBuilder withWorldBackupInterval(int seconds)
		{
			this.worldBackupInterval = seconds * 1000;
			return this;
		}
		
		public BackupConfigBuilder withCompleteBackup(boolean isCompleteBackup)
		{
			this.isCompleteBackup = isCompleteBackup;
			return this;
		}
		
		public BackupConfigBuilder withCompleteBackupInterval(int seconds)
		{
			this.completeBackupInterval = seconds * 1000;
			return this;
		}
		
		public BackupConfig build()
		{
			return new BackupConfig(location, isWorldBackup, worldBackupInterval, 
					isCompleteBackup, completeBackupInterval);
		}
	}
}
