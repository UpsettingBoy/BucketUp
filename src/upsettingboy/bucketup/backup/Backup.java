// Copyright (c) 2018 UpsettingBoy (Jerónimo Sánchez)
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package upsettingboy.bucketup.backup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import upsettingboy.bucketup.Main;
import upsettingboy.upsetcore.io.ConfigUtils;
import upsettingboy.upsetcore.io.ConsoleUtils;
import upsettingboy.upsetcore.io.FileUtils;

public class Backup
{		
	private String location;
	
	private BackupConfig globalConfig;
	
	private static boolean isInitialized = false;
	
	private byte[] buffer = new byte[1024];
	
	private boolean hadWarning = false;
	
	private Path rootFolder = new File(FileUtils.getServerLocation()).toPath();

	private Timer t;

	private ArrayList<TimerTask> lTimerTasks = new ArrayList<TimerTask>();
	
	public Backup()
	{
		globalConfig = ConfigUtils.<BackupConfig>readConfig(Main.CONFIG_FILE, BackupConfig.class);
		
		location = globalConfig.getBackupLocation();
	}
	
	public void start()
	{
		if(isInitialized)
		{
			ConsoleUtils.Warning(Main.PLUGIN_NAME, "Backup task is already started!");
			return;
		}
		
		isInitialized = true;		
		
		t = new Timer();

		if(globalConfig.isCompleteBackup())
		{
			TimerTask task = new TimerTask() 
			{
					
				@Override
				public void run() 
				{
					makeBackup(new File(FileUtils.getServerLocation()).listFiles(), true);
				}
				
			};		
			
			t.schedule(task, 0, globalConfig.getCompleteBackupInterval());
			
			lTimerTasks.add(task);
		}
		
		if(globalConfig.isWorldBackup())
		{
			t = new Timer();
			TimerTask task = new TimerTask() 
			{
					
				@Override
				public void run() 
				{
					File[] worlds = new File(FileUtils.getServerLocation()).listFiles(new FilenameFilter() {
						
						@Override
						public boolean accept(File file, String name)
						{
							String lowName = name.toLowerCase();
							if(file.isDirectory())
							{
								if(lowName.startsWith(getWorlName().toLowerCase()))
									return true;
							}
							
							return false;
						}
					});
					
					makeBackup(worlds, false);
				}
				
			};		
			
			t.schedule(task, 0, globalConfig.getWorlBackupInterval());

			lTimerTasks.add(task);
		}
	}
	
	@SuppressWarnings("unused")
	private String getWorlName()
	{
		String fileConfig = FileUtils.getServerLocation() + File.separator + "server.properties";
		String keyPropertie = "level-name";
		BufferedReader reader = null;
		
		String value = "";
		
		try
		{
			reader = new BufferedReader(new FileReader(new File(fileConfig)));
			
			String line;
			while((line = reader.readLine()) != null)
			{
				if(line.startsWith(keyPropertie))
				{
					value =  line.split("=")[1];
					break;
				}
			}
		}
		catch(Exception e)
		{
			ConsoleUtils.Error(Main.PLUGIN_NAME, e.getMessage());
		}
		finally
		{
			try
			{
				if(reader != null)
					reader.close();
			}
			catch(Exception i)
			{
				ConsoleUtils.Error(Main.PLUGIN_NAME, i.getMessage());
			}
		}
		return value;
	}

	public void forceBackup()
	{
		for (TimerTask var : lTimerTasks) 
			var.run();
	}

	public void disableTimers()
	{
		t.cancel();
		isInitialized = false;
		lTimerTasks.clear();
	}
	
	private void makeBackup(File[] backup, boolean isComplete)
	{
		String coso = isComplete? "Complete" : "World";
		
		ConsoleUtils.Log(Main.PLUGIN_NAME, String.format("Making %s backup!", coso));
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH-mm-ss");
		LocalDateTime now = LocalDateTime.now();
		
		String logFile = location + File.separator + String.format("[%s] ", coso) + dtf.format(now);
		
		File backupLocation = new File(logFile + ".zip");
		FileUtils.createFileParentPath(backupLocation);
		
		File[] files = backup;
		
		try
		{
			StringBuilder builder = new StringBuilder();
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(backupLocation));
			
			for(File f : files)
				makeBackupRecursive(f, out, builder);
			
			if(hadWarning)
			{
				ConsoleUtils.Warning(Main.PLUGIN_NAME, "Backup end with some warnings");
				
				FileWriter writer = new FileWriter(new File(logFile + ".log"));
				writer.write(builder.toString());
				
				writer.close();
			}
			
			out.closeEntry();
			out.close();
		}
		catch(Exception e)
		{
			ConsoleUtils.Error(Main.PLUGIN_NAME, e.getMessage());
		}
		
		ConsoleUtils.Log(Main.PLUGIN_NAME, "Backup finished!");
	}
	
	@SuppressWarnings("unused")
	private void makeBackupRecursive(File f, ZipOutputStream out, StringBuilder builder) throws IOException
	{
		if(f.isDirectory())
		{
			File[] files = f.listFiles();
			for(File fil : files)
				makeBackupRecursive(fil, out, builder);
		}
		else
		{
			FileInputStream stream = null;
			try
			{
				ZipEntry entry = new ZipEntry(rootFolder.relativize(f.toPath()).toString());
				out.putNextEntry(entry);
				
				stream = new FileInputStream(f);
				
				int len;
				while((len = stream.read(buffer)) > 0)
					out.write(buffer, 0, len);
			}
			catch(Exception i) 
			{
				builder.append(String.format("[%s] %s %s", Main.PLUGIN_NAME, i.getMessage(), System.lineSeparator()));
				
				if(!hadWarning)
					hadWarning = true;
			}
			finally
			{
				if(stream != null)
					stream.close();					
			}
		}
	}
}
