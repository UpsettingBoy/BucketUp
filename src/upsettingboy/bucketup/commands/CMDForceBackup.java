// Copyright (c) 2018 UpsettingBoy (Jerónimo Sánchez)
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package upsettingboy.bucketup.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import upsettingboy.bucketup.Main;

public class CMDForceBackup implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) 
    {      
       if(arg0 instanceof ConsoleCommandSender || (arg0 instanceof Player && ((Player)arg0).isOp()))
       {
            Main.getBackupInstance().forceBackup();
            return true;
       }

       return false;
	}

}